#include <tins/tins.h>
#include <iostream>
#include <stddef.h>

using namespace Tins;

size_t counter(0);

bool packet_info(const PDU &pdu)
{
	const TCP* tcp = pdu.find_pdu<TCP>();
	const IP* ip = pdu.find_pdu<IP>();
	const RawPDU* rpdu = pdu.find_pdu<RawPDU>();

	counter++;

	std::cout << counter << ".packet-------------------------------" << std::endl;
	
	// ip
	std::cout << "ip sorce addr: " << ip->src_addr() << std::endl;
	std::cout << "ip dst addr: " << ip->dst_addr() << std::endl;

	// tcp
	std::cout << "tcp header_size: " << tcp->header_size() << std::endl;
	std::cout << "tcp src port: " << tcp->sport() << std::endl;
	std::cout << "tcp dst port: " << tcp->dport() << std::endl;
	std::cout << "tcp len: " << rpdu->header_size() << std::endl;
	
	// raw pdu
	std::cout << "packet size: " << pdu.size() << std::endl;
	std::cout << "end -------------------------------\n" << std::endl;
	return true;
}

int main()
{
	FileSniffer sniffer("/root/Documents/net_study/libtins_study/part2/homework2.pcap");
	sniffer.sniff_loop(packet_info);

	std::cout << "There are " << counter << " packets in the pcap file\n" << std::endl;
}