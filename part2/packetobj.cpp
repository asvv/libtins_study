#include <vector>
#include <tins/tins.h>

using namespace Tins;

int main()
{
	std::vector<Packet> vt;

	Sniffer sniffer("eth0");

	while(vt.size() != 10)
	{
		vt.push_back(sniffer.next_packet());
	}

    std::cout << "TIME \t\t SRC IP \t\t DST IP" << std::endl;

	for(const auto& packet : vt)
	{
		if(packet.pdu()->find_pdu<IP>())
		{
			std::cout << packet.timestamp().seconds() << "\t"
                << packet.pdu()->rfind_pdu<IP>().src_addr() << "\t\t"
                << packet.pdu()->rfind_pdu<IP>().dst_addr()
                << std::endl;
		}
	}
}