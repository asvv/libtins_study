#include <tins/tins.h>

using namespace Tins;

int main()
{
	PacketSender sender;
	IP pkt = IP("8.8.8.8") / UDP(53, 1337) / DNS();

	// DNS query: Name, Type, Class
	// Type is A : Host Address
	// Class is IN : Internet Class
	pkt.rfind_pdu<DNS>().add_query({ "www.google.com", DNS::A, DNS::IN });

	pkt.rfind_pdu<DNS>().recursion_desired(1);

	// send and recv
	std::unique_ptr<PDU> response(sender.send_recv(pkt));

	// recv result
	if(response)
	{
		DNS dns = response->rfind_pdu<RawPDU>().to<DNS>();
		// answers
		for( const auto &record : dns.answers() )
		{
			std::cout << record.dname() << " - " << record.data() << std::endl;
		}
	}
}