#include<tins/tins.h>
#include<cassert>
#include<iostream>
#include<string>

using namespace Tins;
using namespace std;

int main()
{
	NetworkInterface iface = NetworkInterface::default_interface();

	NetworkInterface::Info info = iface.addresses();	// mac address

	EthernetII eth = EthernetII("77:22:33:11:ad:ad", info.hw_addr) /
	 IP("192.168.0.1", info.ip_addr) /
	 TCP(13, 15) /
	 RawPDU("payload!");

	PacketSender sender;

	sender.send(eth, iface);
}
