#include <tins/tins.h>
#include <thread>
#include <chrono>

using namespace Tins;

RadioTap MyBeacon(std::string ssid);

int main()
{
	RadioTap outer1 = MyBeacon("멘토님의이불킥");
	RadioTap outer2 = MyBeacon("길길멋있다");
	RadioTap outer3 = MyBeacon("길길짱잘생김");

	PacketSender sender;
	while(true)
	{
		sender.send(outer1, "wlan0");
		sender.send(outer2, "wlan0");
		sender.send(outer3, "wlan0");
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
}

RadioTap MyBeacon(std::string ssid)
{
	Dot11Beacon beacon;
	beacon.addr1(Dot11::BROADCAST);
	beacon.addr2("00:01:02:03:04:05");
	beacon.addr3(beacon.addr2());
	beacon.ssid(ssid);

	beacon.ds_parameter_set(8);
	beacon.supported_rates({ 1.0f, 5.5f, 11.0f });
	beacon.rsn_information(RSNInformation::wpa2_psk());

	RadioTap outer;
	outer.inner_pdu(beacon);
	return outer;
}